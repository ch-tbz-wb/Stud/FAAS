# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen

 * B10 Softwarearchitektur analysieren und bestimmen  
   * B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren
   * B10.2 Applikationen unter Beachtung übergeordneter Konzepte wie der ICT-Strategie, Standards etc. in die Softwarearchitektur integrieren

 * B11 Applikationen entwickeln, Programme erstellen und testen
   * B11.1 Vorgaben für die Konzipierung eines Softwaresystems mit einer formalen Methode analysieren  
   * B11.2 Systemspezifikation interpretieren und die technische Umsetzung entwerfen  
   * B11.3 Spezifikation in einer geeigneten Programmiersprache umsetzen  

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren

## Allgemeine Handlungskompetenzen
 * B5 ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren
   * B5.1 ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern
   * B5.10 ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen
   * B5.2 Sich gegenseitig beeinflussende Faktoren berücksichtigen und Veränderungen antizipieren

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
