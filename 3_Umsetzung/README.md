# Umsetzung
 - Bereich: Applikationen entwickeln, Programme erstellen und testen
 - Semester: 2

## Lektionen: 

* Präsenzunterricht: 50
* Fernunterricht: 50
* Selbststudium: 60

## Lernziele
 - Grundlegende Begriffe und Definitionen von Python verstehen
 - Logik und Struktur von Python verstehen
 - Literale und Variablen in den Code einführen und verschiedene Zahlensysteme verwenden
 - Operatoren und Datentypen wählen, die zum Problem passen
 - Konsolen-Eingabe/Ausgabe-Operationen durchführen
 - Entscheidungen treffen und den Ablauf mit der if-Anweisung verzweigen
 - Verschiedene Arten von Iterationen durchführen
 - Daten mit Listen sammeln und verarbeiten
 - Daten mit tuples sammeln und verarbeiten
 - Daten mit dictionaries sammeln und verarbeiten
 - Mit Strings arbeiten
 - Den Code mit Funktionen zerlegen
 - Die Interaktion zwischen der Funktion und ihrer Umgebung organisieren
 - Code Strukturieren
 - Python Built-In Exceptions Hierarchy
 - Grundlagen der Python Exception Handling
 - HTTP API mit Flask bereitstellen
 - In Datei und Konsole protokollieren
 - Nachrichtenaustausch mit MQTT
 - Service als Docker Container bereitstellen

## Voraussetzungen

Modul [122 Abläufe mit einer Scriptsprache automatisieren](https://www.modulbaukasten.ch/module/122) aus der Grundbildung

## Dispensation

Für den ersten Teil:
[PCEP - Certified Entry-Level Python Programmer](https://pythoninstitute.org/pcep) oder höher

## Technologien 

Python, PIP, PyCharm, Flask, MQTT, Visual Studio Code, Jupyter, Docker

## Methoden

Self-Learning Plattformen und praktische Laborübungen, Coaching durch Lehrperson

## Schlüsselbegriffe

Python, Programmieren, IDE, FAAS, Skriptsprache, Objektorientierte Programmierung (OOP), Entwicklungsumgebung, Automatisierung

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Alle Lehrmittel sind in diesem Repository verlinkt. 






