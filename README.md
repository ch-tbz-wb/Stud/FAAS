![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# FAAS - Function as a Service / Python

## Kurzbeschreibung des Moduls 

Python ist eine moderne Programmiersprache, welche sich grosser Beliebtheit in zahlreichen wissenschaftlichen und technischen Bereichen erfreut. In jüngerer Zeit hat sich Python als eine der führenden Sprachen in den Bereichen Data Science, maschinelles Lernen, Datenanalyse, Automatisierung und der breiten Softwareentwicklung etabliert. Dieser Einführungskurs bietet den perfekten Einstieg in die Programmierwelt.

Im Modul FAAS werden die Grundlagen zu der Programmierung mit Python vermittelt. Dies umfasst ein breites Spektrum an Themen, die für die effektive Nutzung von Python unerlässlich sind. Zunächst werden die Teilnehmer mit den Basiskonzepten der Sprache vertraut gemacht, wie Variablentypen, Datenstrukturen (Listen, Tuples, Dictionaries, Sets), Kontrollstrukturen (Schleifen und bedingte Anweisungen), Funktionen und Modul-Management.

Des Weiteren wird ein Einblick in objektorientierte Programmierung gegeben, um Konzepte wie Klassen, Objekte, Vererbung und Polymorphismus zu verstehen. Grundlegende Fehlerbehandlung und Ausnahmebehandlung sind ebenfalls ein wichtiger Bestandteil des Kurses, um robuste und fehlertolerante Programme zu schreiben.

Im zweiten Teil des Kurses werden weiterführende Themen wie JSON, YAML, Flask, MQTT, Function As a Service an Hands-On Projekten behandelt. 

## Angaben zum Transfer der erworbenen Kompetenzen 

 - Self-Study Python Kurs mit Coaching durch Lehrperson
 - Praktische Übungen und Challenge Aufgaben mit Pycharm und Jupyter

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

- IAC: Programmierkenntnisse
- AWS & Azure: Cloud-Umgebungen mit FaaS

### Nachfolgende Module


## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
