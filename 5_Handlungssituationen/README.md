# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.

### 1. Erstellen eines einfachen Rest-APIs mittels FaaS

Bsp für Handlungssituation: ich kann ...

### 2. Erstellen sie eine Funktion welche einen DNS-Service überwacht und alarmiert

Bsp für Handlungssituation: ich kann ...
