# UML-Design-Prozess des Spiels TicTacToe

Wir beginnen ganz einfach und überlegen uns, was beim Spiel passiert → Version 1 des Use-Case-Diagramms:

![Use-Case-V1](./resourcen/tictactoe_1-UML-Use-Case.drawio.png)

Dann schauen wir im Use-Case-Diagramm, welche Klassen es dazu braucht, und erstellen Version 1 des Klassendiagramms:

![Class-Diagramm-V1](./resourcen/tictactoe_1-UML-Class-Diagram.drawio.png)

- Es braucht eine Main-Klasse, um das Programm zu starten.
- Game soll den Spielstand speichern und ermitteln, wer gewonnen hat.
- Player soll die Eingaben des Spielers entgegennehmen und eventuell noch Angaben zum Spieler enthalten.
- ComputerPlayer soll die Züge des Computers ausführen.

Das Ganze ist noch nicht optimal. Wer übernimmt die Aufgabe, das Ganze "grafisch" aufzubereiten und darzustellen? Game? Ich würde sagen, wir fügen eine zusätzliche Klasse `GameBoard` dafür ein.

![Class-Diagramm-V2](./resourcen/tictactoe_2-UML-Class-Diagram.drawio.png)

Welche Konsequenzen hat das für unser Use-Case-Diagramm? Wir benötigen noch einen Use-Case, der das Zeichnen übernimmt:

![Use-Case-V2](resourcen/tictactoe_2-UML-Use-Case.drawio.png)

Okay, jetzt können wir das GameBoard zeichnen. Aber wie wird das GameBoard nach jedem Zug aktualisiert, d.h. wie kommen die Züge ins GameBoard? Vielleicht brauchen wir dazu einen neuen Use-Case. Sagen wir `update GameBoard`:

![Use-Case-V3](resourcen/tictactoe_3-UML-Use-Case.drawio.png)

Jetzt scheint alles gut zu sein. Aber wie sieht es beim Klassendiagramm aus? Soll wirklich `Main` mit dem `Player` kommunizieren?

Vielleicht ist es doch besser, wenn das `Game` diese Aufgabe übernimmt.

![Class-Dia-V3](resourcen/tictactoe_3-UML-Class-Diagram.drawio.png)

Jetzt sind wir an dem Punkt, an dem wir uns Gedanken darüber machen sollten, wie genau ein Zug eines Spielers und ein Zug des ComputerPlayers ablaufen soll. Dazu erstellen wir ein Sequenzdiagramm:

Hier für den Player:

![Seq-Dia-Player-V3](resourcen/tictactoe_3-Sequenz-Diagramm-Player-Make-Move.drawio.png)

Das scheint in Ordnung zu sein. Außer vielleicht, dass der Player einfach ein Feld eingeben kann, das schon besetzt ist. Aber das könnten wir lösen, indem wir `getMove()` eine Liste der verfügbaren Felder übergeben. Doch wo haben wir diese gespeichert?

Hier für den ComputerPlayer:

![Seq-Dia-ComputerPlayer-V3](resourcen/tictactoe_3-Sequenz-Diagramm-Computer-make-move.drawio.png)

Es gibt eine Methode für den ComputerPlayer, die den nächsten Zug berechnet: `calculateNextMove()`. Aber auch diese müsste wissen, wie das aktuelle Board aussieht. Sollen wir einfach das Board als Array oder das gesamte `GameBoard` übergeben?

Jetzt müssen wir überlegen, welche Attribute wir wo unterbringen, damit wir das dann einfacher programmieren können. Das tun wir wieder im Klassendiagramm:

- Attribute von `Game`: `gameBoard`, `player`, `computerPlayer`
- Player: keine oder vielleicht `Name` oder `Emoji` zur Darstellung?
- ComputerPlayer: keine oder vielleicht `Level` und `Emoji` zur Darstellung?
- GameBoard: `fields` (Array, in welchem entweder der ComputerPlayer oder der Player steht)

Dann kommen wir zu diesem Klassendiagramm:

![Class-Dia-V4](resourcen/tictactoe_4-UML-Class-Diagram.drawio.png)

Wenn wir jetzt annehmen, dass das `GameBoard` das Zeichnen übernehmen soll, muss es anhand der `fields` für jedes Feld das richtige Zeichen wissen. Das ist aber im `Player` und `ComputerPlayer` gespeichert. Vielleicht wäre es sinnvoller, `fields` direkt als ein Array von `Playern` und `ComputerPlayern` zu definieren. Aber es gibt auch noch leere Felder, d.h. wir müssten dann noch eine Klasse für leere Felder einführen. Also tun wir das:

![Class-Dia-V5](resourcen/tictactoe_5-UML-Class-Diagram.drawio.png)

Jetzt bleibt nur noch die Frage, wie wir vom `GameBoard` auf das Attribut `emoji` zugreifen können. Im Array `fields` gibt es Objekte vom Typ `Object`. `Object` hat jedoch kein Attribut `emoji`. Jetzt wäre es praktisch, wenn ich wüsste, dass jedes der Objekte eine Methode `getEmoji()` hat, die das Emoji zurückgibt. Genau das erreichen wir mit einem Interface `Emoji`, das wir nun noch hinzufügen.

![Class-Dia-V6](resourcen/tictactoe_6-UML-Class-Diagram.drawio.png)

An diesem Punkt würde ich anfangen zu programmieren, da ich wahrscheinlich schon in der Lage bin, das Board zu zeichnen. Ich kann wahrscheinlich auch den "Player macht Zug" Use-Case programmieren.

Vielleicht muss ich noch etwas anpassen für den "ComputerPlayer macht Zug" Use-Case. Aber das sehen wir später.

Es wird wahrscheinlich auch einen Use-Case "Choose Emojis" geben, aber den muss ich nicht unbedingt zeichnen, um loslegen zu können.
