# A - Einführung in Python

[TOC]

Python ist eine der gängigsten Programmiersprachen, die heute im Backend development benutzt werden (siehe [https://bootcamp.berkeley.edu/blog/most-in-demand-programming-languages](https://bootcamp.berkeley.edu/blog/most-in-demand-programming-languages/)). Darum werden wir im Function as a Service Module als erstens eine kurze Einführung in Python machen.


## Mein erstes Python-Programm

In diesem Kapitel werden wir Python installieren und dazu die richtige IDE (Integrated Development Environment) installieren

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

- Ich kann eine Python-Programmierumgebung aufsetzen
- Ich kann PyCharm bedienen.
- Ich kann mein erstes Programm in der IDE laufen lassen
- Ich kann mein erstes Programm auf der Kommmandozeile (bash oder powershell) laufen lassen.

### Transfer

### Hands-on

#### Installieren von Python

Es gibt verschiedene Quellen und Versionen von Python-Interpretern. Wir benutzen die Standard-Python. Benutzt auf Windows 11 den Microsoft Store und sucht nach Python.
Sonst geht es hier zum [Download](https://www.python.org/downloads/)

#### Installation vom IDE

Die beste Umgebung die ich kenne ist PyCharm von der Firma Jetbrains.
Hier gehts zum [Download](https://www.jetbrains.com/de-de/pycharm/download/)
**PyCharm Community Edition** ist für unsere Zwecke genügend.

#### Mein erstes Python-Programm

Erstellt ein Programm mit folgendem Inhalt. 

```python
#!/usr/bin/env python3

print("Ich kann bis 100 zählen")
for i in range(1,100):
    print(i)

```

Last dieses auf folgende Varianten laufen :

- In PyCharm mit dem Run Icon 
- Im Terminal von PyCharm mit `python main.py`
- Im Terminal von PyCharm mit `./main.py` (dies kann auf Windows nicht funktionieren, je nach Shell)
- In einem Terminal eurer Wahl wie zum Beispiel git-bash. Benutzt dieselben Kommandos wie im Im Terminal.
- In PyCharm mit dem Debug Icon
- Setzt einen Breakpoint und Startet das mit dem Debug Icon.


#### Links mit Tutorials

Hier findet ihr Links mit Tutorials dazu.

- [Run Hello World in PyCharm Tutorial](https://www.guru99.com/creating-your-first-python-program.html)
- [Video: PyCharm Introduction English 11:00](https://www.youtube.com/watch?v=UHQQy_g1d0U)
- [Video: PyCharm Einführung Deutsch 3:33](https://www.youtube.com/watch?v=haM38ANLcRs)
- [Python Debugger starten by dev-insider.de](https://www.dev-insider.de/debugging-von-python-programmen-a-96d04a88c5a34e8a54721f1ea588e466/)
- [PyCharm Debugger benutzen by Jetbrains](https://www.jetbrains.com/help/pycharm/debugging-python-code.html)


## Python Syntax 

Es gibt sehr viele verschiedene Tutrials, die die Syntax von Python einführen.
In diesem Teil werdet ihr anhand von einem Tutorial interaktiv die Syntax von Python kennenlernen. 

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

- Ich kenne die wichtigsten Datentypen von Python
- Ich kenne die wichtigsten Kontrollstrukturen von Python
- Ich weiss wie man Funktionen und Klassen definiert.

### Hands-on

Lerne die Syntax anhand von [PCAP Programming Essentials in Python by CISCO](https://www.netacad.com/courses/programming/pcap-programming-essentials-python)

Falls du schon Erfahrung in Python hast kannst du die folgenden Aufgaben als Knowledge Check machen.
- [Numbers](./Uebungen/01_Numbers.md)
- [Conditionals](./Uebungen/02_Conditionals.md)
- [Strings](./Uebungen/03_Strings.md)
- [Lists](./Uebungen/04_Lists.md)
- [Loops](./Uebungen/05_Loops.md)
- [Functions](./Uebungen/08_Functions.md)
- [Classes](./Uebungen/07_Classes.md)
- [Dicts to JSON](./Uebungen/06_Dicts_to_JSON.md)


Benutzt die Tutorials für den Einstieg. Sobald ihr das Gefühl habt, eine der Übungen lösen zu können, solltet ihr es versuchen. D.h. ihr müsst nicht alle Punkte der Tutorials durchgehen.

## Standard Module, Virtuelle Environments 

In diesem Kapitel werfem wir einen Blick auf die Standard-Module die mit Python3 installiert werden. Wir sehen auch, wie man *Virtual Environments* erzeugt und in diesen zusätzliche Module installiert.

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

- ich kenne die Liste der Standard-Module
- ich kann eines dieser Module anwenden
- ich kann ein venv erzeugen und aktivieren
- ich kann mit pip module nachinstallieren

### Theorie

#### Importieren von Modulen
Die ganze Theorie dazu könnt ihr unter [https://docs.python.org/3/tutorial/modules.html](https://docs.python.org/3/tutorial/modules.html) nachlesen.

Die kurze Variante dazu:

Um alle Funktionen, Klassen und Variablen aus einem Python-File `mymodule.py` zu importieren, dass im gleichen direktory wie euer skript lieght benutzt ihr
```python
import mymodule
```
um dann auf eine Funktion oder Klasse mit dem Namen `myfunc` in diesem Module zuzugreifen benutzt ihr:
```python
mymodule.myfunc()
```

Falls sich das File `mymodule.py` in einem subdirectory `subdir1/subdir2` befindet, importiert und benutzt ihr dieses mit:
```python
import subdir1.subdir2.mymodule
subdir1.subdir2.mymodule.myfunc()
```

Ihr könnt `myfunc()` auch direct im Namespace von eurem Skript importieren und benutzen:
```python
from subdir1.subdir2.mymodule import myfunc
myfunc()
```

Ihr könnt sowohl bei `from ... import` als auch bei `import` dem zu importierenden Modul, Funktion,Klasse oder Variable einen Kurznamen geben und diesen dann in eurem Skript benutzen:
```python
from subdir1.subdir2.mymodule import myfunc as arminfunc
arminfunc()
import subdir1.subdir2.mymodule as arminmodule
arminmodule.myfunc()
```
#### Standard-Module
Standard-Module gibt es viele in Python. Eine aktuelle Liste finden sie unter https://docs.python.org/3/library/index.html
Hier 4 Standard-Module die ihr sicher früher oder später brauchen werdet:
- JSON parsen: [https://docs.python.org/3/library/json.html](https://docs.python.org/3/library/json.html) (einfaches Modul)
- Commandline-Argumente Parsen: [https://docs.python.org/3/library/argparse.html](https://docs.python.org/3/library/argparse.html) (mittleres Modul)
- HTTP-Requests machen: [https://docs.python.org/3/library/urllib.request.html](https://docs.python.org/3/library/urllib.request.html) (einfaches bis sehr komplexes Modul)
- Logfiles schreiben: [https://docs.python.org/3/library/logging.html](https://docs.python.org/3/library/logging.html) (einfach bis sehr komplexes Modul)

#### Virtual Environments (venv) und Package installer for Python (pip)

Machmal genügen die Standard-Module von Python nicht. Dann hat man eine grosse Auswahl von Python-Packages die man nachinstallieren kann. Eine Liste findet man unter [https://pypi.org/](https://pypi.org/).
Da man aber für jede Projekt andere Packages braucht, will man diese nicht immer alle installiert haben, sondern nur die nötigen. Dazu gibt es [Virtual Environments(venv)](https://docs.python.org/3/library/venv.html).

Beim Erstellen einen neuen Projektes wird bei PyCharm immer die Möglichkeit geboten, den Python-Interpreter auszuwählen oder sogar direkt ein venv zu erzeugen, falls man das wünscht.
Danach kann man in PyCharm direkt unter Preferences -> Project:... -> Python Interpreter Packages installieren. Oder man öffnet das Terminal in PyCharm und gibt 
`pip install <packagename>` ein.

### Hands-on

Diese Übungen zeigen euch wie ihr ohne Hilfe von PyCharm ein `venv` erzeugt und aktiviert und darin mit `pip` Module installiert.

- [Importieren von Modulen, Klassen und Funktionen](./Uebungen/10_import.md)
- [Erstellen und nutzen von Virtual Environment](./Uebungen/12_venv.md)
- [Installieren von Modulen mit pip](./Uebungen/15_pip.md)
- [AWS-SDK benutzen](./Uebungen/20_awssdk.md)

### Links

*Links zu den Unterthemen*



##  Programmier-Paradigmen

Es gibt verschiedene Arten zu Programmieren. Diesen sagt man auch Paradigmen.

Die wichtigsten Paradigmen sind
- Prozedurale Programmierung
  - Strukturierung des Codes um Funktionen herum, um eine klare Trennung von Aufgaben zu ermöglichen.
- Objektorientierte Programmierung
  - Programmierung um Objekte, die Instanzen von Klassen sind, herum. Es betont Eigenschaften und Verhalten der Objekte.
  - Es werden Daten und Methoden in einem Objekt zusammengefasst
- Funktionale Programmierung
  - Fokus auf Funktionen als Grundbausteine, die keine Seiteneffekte haben und immutable Daten verwenden.

Hier möchte ich konkreter auf Objektorientierte Programmierung eingehen. Dazu gibt es tausende von Büchern, Videos und Dokumente, darunter bessere und schlechtere. 

Hier eine [Kurzeinführung](https://pynative.com/python/object-oriented-programming/)

Noch kürzer:

- Warum tun wir es?​​​​​​​
    Wir wollen komplexen Code möglichst übersichtlich in kleinen Portionen programmieren können.
- Wie tun wir es?
  - **Abstraktion:** Wir nehmen möglichst reale Objekte in unserem System und versuchen diese in Klassen zu abstrahieren.
  - **Inheritance:** Man kann gewisse Sachen für ähnliche Klassen in einer Parentklasse erledigen, d.h. für alle Subklassen kann man dasselbe tun.
  - **Polimorphismus:** Auch soll man ähnliches Verhalten für ähnliche Objekte (Zum Beispiel die Abfrage des Status einer VM auf AWS und Azure) zwar in verschieden Klassen unterbringen, aber der Methoden-Aufruf und die Rueckgabewerte so gestallten, dass sie gleich benutzt werden koennen. -> if ... else statements nur an wenigen stellen im code noetig
  - **Delegation:** Der Trick ist, kleine aber feine Klassen zu schreiben die zusammenarbeiten, anstatt viel Code in einer Funktion oder Klasse unterzubringen.
  - **Encapsulation:** Zusammenführen von Daten und Code (Bsp: Verbindung zu AWS-Rest-APIs mit einem Objekt der Klasse boto3.client) in Objekten. Man kann verschiedene Credentials benutzen und jeweils ein boto3.client Objekt erzeugen. Dann kann man im gleichen Code für mehrere verschiedene "Credetials" Resourcen (VMs,subnets, security groups,....) erzeugen. Dazu hat man dann einfach verschiedene Client-Objekte.

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

- Ich kann einfache Klassen definieren, die eine Teilfunktion des Systems übernehmen.

### Transfer

### Hands-on

#### Aufgabe 1

Wie gestallte ich ein Überwachungsprogramm das ganz verschiedene Parameter einer AWS-Instanz überwachen kann.
![Classdiagramm](./resourcen/oop_classdiagramm.drawio.png)

Das Beispiel Main-Programm sieht ziemlich schlank aus.
Es benutzt die Objekte in der Zeichnung(Klassendiagramm).

Ihr sollt die Grundgerüste der Klassen so bauen, dass dieser Code ausgeführt werden kann.
Definiert die Klassen so, dass sie instanziert werden können, aber noch nichts wirklich tun.

#### Aufgabe 2

Implementiert die Methode:
`InstanzChecker.check_and_notify()`

Benutzt dabei die `XyzTest`-Klassen und die `AbcNotifier`-Klassen bzw. deren Methoden `XyzTest.check()` und `AbcNotifier.notify()` um das einfach zu erreichen.

Erzeugt die Objekte der Klassen `XyzTest` innerhalb des Konstruktors `__init__()` von `InstanzChecker`.
Speicher diese in einem Privaten Attribute `self.__tests`.


#### Aufgabe 3

Implmentiert die `AbcNotifier`-Klassen nur soweit, dass ausgeben wird was gemacht würde wie zum Beispiel für `EmailNotifier`: 
```
print('Hier wuerde ein Email an %s versendet.'%self.emailAdresse)
print('Das Subject waere: %s'%subject)
print('Der Inhalt waere: %s'%message)
```

#### Aufgabe 4

Versucht nun den Code auszuführen

#### Lösung
Hier eine [mögliche Lösung](https://gitlab.com/armindoerzbachtbz/loesungen_python/-/tree/main/oop?ref_type=heads) für das Problem.
### Links

*Links zu den Unterthemen*


## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

## AWS-Lambda FaaS


In diesem kurzen Kapitel möchte ich euch den Übergang zu **FAAS** erleichtern.

Dies möchte ich mit folgenden Auftrag tun:

- [Show My IP](./Uebungen/30_showmyip.md)

## Exkurs über Object-Oriented-Analysis(OOA) and Design (OOD)

Hier möchte ich einfach anhand vom [Beispiel TicTacToe](./UML_Desing_of_TicTacToe.md) euch die Art wie man Object-Orientiert programmiert näher bringen und ganz speziell, wie man zu den richtigen Klassen und Methoden kommt, zeigen.

### Links zu weiterführenden Erklärungen
https://de.wikipedia.org/wiki/Objektorientierte_Analyse_und_Design

### Hands-On
Nehmt zu zweit oder zu dritt ein Problem das ihr Programmieren wollt und versucht mit diesem Vorgehen zu einem einigermassen richtigen UML-Klassendiagramm zu kommen.

