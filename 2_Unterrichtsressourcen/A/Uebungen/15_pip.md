# Package Installation for Python (pip)

Es gibt viele kleine Packages für Python, welche auf [pypi.org](http://pypi.org) heruntergeladen werden können. Dazu gibt es das tool `pip`. Mit ihm werden die Packages heruntergeladen und im `venv` oder auf dem System direkt installiert.

1) Erzeugt ein neues `venv` und activiert es.
2) Versucht einmal `pip install chatgpt-gui` in einem Virtual Environment zu installieren.
3) Jetzt könnt ihr `chatgpt` starten und es sollte ein GUI starten.
4) Jetzt koennt ihr das GUI testen.
5) Loescht das `venv` einfach indem ihr es mit `rm -r <directory_of_venv>` löscht. 

So kann man für jedes Python-Projekt das eigene `venv` machen und man stört die anderen Projekte nicht mit zusätzlichen inkompatiblen Packages.
