Die Loesung für die Lambda funktion ist:
```python
import json

print('Loading function')


def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    response={
        'statusCode':200,
        'headers': { 'Content-Type' : 'text/html; charset=utf-8' },
        'body':f"<H1>Your IP is:{event['requestContext']['identity']['sourceIp']}</H1><pre>{json.dumps(event,sort_keys=True, indent=4)}</pre>"
    }
    return response
```
