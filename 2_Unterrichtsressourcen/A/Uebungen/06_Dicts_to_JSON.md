# Übungen zu Dicts und JSON

## Aufgabe 1: JSON als String in dict umwandeln

Als erstes mit Hilfe vom Modul json-File [06_Dicts_to_JSON.input.json](./resourcen/06_Dicts_to_JSON.input.json) einlesen.

als erstes müsst ihr ein File zum lesen öffnen und lesen geschieht mit:

```python
with open(filename,"r") as inputfile:
    inputstring=inputfile.read()
    .... hier kommt die Umwandlung vom inputstring zum dict .....
``` 
Die Umwandlung vom String zum dict geschieht dann mit dem [Python-Modul json](https://docs.python.org/3/library/json.html). Versucht mal aus der [Dokumentation](https://docs.python.org/3/library/json.html) selbst herauszufinden, welche Funktion ein `string` in JSON umgewandelt.

## Aufgabe 2: Feld aus dict ausgeben

Gebt aus dem `dict` jetzt das Feld `city` aus.


## Aufgabe 3: Ein JSON für einen API-Call vorbereiten

Das API das ihr benutzen möchtet erwartet als Payload eines POST-Requests ein JSON mit folgendem Format:
```json
{
    "name": "Peter", 
    "surname": "Muster", 
    "age": 53, 
    "hobbies": ["Skifahren", "Segeln", "Klettern"]
}
```

Ihr sollt jetzt eine Variable inp vorbereiten die einen `dict` enthält, welcher diesem JSON output entspricht.

## Aufgabe 4: JSON-String aus dict herstellen

Stellt jetzt aus dem `dict` aus Aufgabe 2 einen JSON-String mit dem Variablennamen `jsondict` her und gebt diesen dann aus.

## Aufgabe 5: Liste als JSON-String
Jetzt sollt ihr nur die Liste der Hobbies als JSON-string mit Variablennamen `jsonlist` herstellen und ausgeben. 


