# Conditionals
Ist ist immer mal wieder nötig Code nur auszuführen wenn gewisse Bedingung erfüllt sind. Dazu gibt es *Conditionals*.

die Keywords sind `if`,`else`,`elif`.

Erweitert das Programm von der Übung Numbers und erzeugt folgenden Output:
- wenn die `eingebene_zahl` nicht numerisch ist, soll `Es wurde keine Zahl eingegeben` ausgegeben werden (Tip: `eingebene_zahl.isnumeric()`)
- wenn die Zahl >= 100 ist soll `Das ist aber eine grosse Zahl` ausgegeben werden
- wenn die Zahl < 0 ist soll `Die Zahl ist negativ` ausgegeben werden
- wenn die Zahl zwischen 10 und 99 ist dann soll `Die Zahl hat 2 Stellen` ausgegeben werden
- wenn die Zahl zwischen 0 und 9 ist dann soll `Die Zahl hat eine Stelle` ausgegeben werden