# Importieren von Module, Klassen und Funktionen

Schaut euch zuerst die [Theorie](/2_Unterrichtsressourcen/A#theorie) dazu an.


## Übung 1

Erstellt ein eigenes Modul-File `mymodul.py` und darin eine Funktion `testfunction()` welche `ich bin testfunction()` ausgibt.

## Übung 2

Importiert die `mymodul.testfunction()` vom Skript `main.py` und führt diese aus.

## Übung 3

Erstellt ein Unterverzeichnis `mypackage` und moved mymodul.py in dieses. Was müsst ihr in main.py ändern damit es wieder funktioniert?


