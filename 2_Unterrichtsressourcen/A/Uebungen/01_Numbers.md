# Übung zu Numbers

Zahlen sind in jeder Programmiersprache vorhanden. In dieser Übung einen Input entgegegennehmen und die Zahl dann in verschiedenen Formaten ausgaben.

Zuerst muesst ihr einen Input entgegennehmen. Dazu gibt es eine Funktion `input` in Python:

```python
eingebenene_zahl=input("Geben sie eine Zahl ein:")
```

Dieses Beispiel gibt zuerst als Prompt `Geben sie eine Zahl ein:` aus und wartet dann auf euren input. Eure Eingabe wird dann in der Variable `eingebenene_zahl` gespeichert.
Allerdings kann man eingeben was man will, da `input` nicht prüft ob es sich wirklich um eine Zahl handelt.

Jetzt könnt ihr versuchen diese Zahl auszugeben. Dazu benutzt ihr `print`

```python
print("Meine Zahl ist "+eingebenene_zahl)
```

Jetzt könnt ihr versuchen diese Zahl wirklich als Integer zu interpretieren. Dazu benutzt ihr `int(eingebenene_zahl)` und versorgt das Resultat in der Variable  `eingebene_integerzahl`

Was passiert wenn ihr etwas anderes als eine Integerzahl eingebt?

Jetzt könnt ihr noch versuchen die Zahl als float zu interpretieren. Dazu benutzt ihr anstatt `int` einfach `float`.




