# Übung zu Functions

Wie ihr ja schon gesehen habt, könnt ihr Funktionen ganz einfach definieren:

```
def funktions_name(parameter_name,...):
    ```Dokumenationskommentar (alias Doc-Strings)```
    ... hier kommt euer code ...
```

# Aufgabe 1: Named Parameters

Beim Aufruf der folgenden Funktion

```python
def myfunction(vorname,telefonnummer,adresse,ort):
    print(f"{vorname} wohnt in {ort} an der {adresse} und hat die Telefonnummer {telefonnummer}")
```
gibt es 2 Möglichkeiten beim Aufruf die Parameter zu setzen.

Mann kann einfach die Parameter in der Reihenfolge, wie sie in der Funktion definiert sind, zu übergeben:
```python
myfunction("Peter",
    "079123132",
    "Zuerichstrasse 1",
    "Baden")
```
oder man benutzt *named Parameters* und kann dann die Reihenfolge selbst wählen:
```python
myfunction(vorname="Peter",
    adressse="Zuerichstrasse 1",
    ort="Baden",
    telefonnummer="079123132")
```

Definiert eine Funktion `power` welche die paramter `base` und `exponent` uebernimmt und folgendes zurückgiebt `base ** exponent`
Und benutzt diese um 2 hoch 10 zu berechnen. Macht 2 verschiedene Aufrufe, einmal mit *named Parameters* einmal ohne *named Parameters*.


# Aufgabe 2: Default-Werte definieren

Oft will man bei einer Funktion Parameterwerte definieren, welche gesetzt werden wenn der Parameter nicht übergeben wird. dann kann man einfach die `def`-Zeile wie folgt ändern.

```python 
def myfunction(vorname,telefonnummer='unbekannt',adresse='unbekannt',ort='unbekannt')
```

Jetzt kann man myfunction nur mit dem Parameter `vorname` aufrufen. Die anderen 3 werden dann den Wert `unbekannt` haben. D.h.

```python
myfunction("Peter"
    )
```
Macht dasselbe wie:

```python
myfunction("Peter",
    "unbekannt",
    "unbekannt",
    "unbekannt")
```

Definiert in der `power`-Funktion von euch den Default-Wert für den `exponent` auf `1` und rechnet `power(10)` aus.

# Aufgabe 3: Unbekannt Anzahl von Parameter übernehmen

Machmal weiss man beim Programmieren einer Funktion nicht genau wieviele Parameter man übergeben bekommt und wie diese heissen sollen. Um diese zu übernehmen kann man folgendes tun:

```python
def myfunction(*positional_parameters, **named_parameters):
    count = 0
    # Gebe alle Parameter ohne namen aus
    for v in positional_parameters:
        print(f"Parameter mit Position {count}={v}")
        count += 1
    # Gebe alle Named Parameter aus
    for k, v in named_parameters.items():
        print(f"key={k} value={v}")

```

Definiert eine Funktion `stringconcat` die eine Variable Anzahl von Positionsparametern nimmt und diese zu einem String zusammenfügt und zurueckgibt. **Achtung** nicht alle Parameter die übergeben werden müssen Strings sein, d.h. ihr müsst diese mit `str()` in einen String umwandeln.

Ihr solltet sie ohne Fehler so aufrufen können:

```python
stringconcat("asdfas",1,3.4,[1,3],int, float, str)
```
und sie sollte etwa das als string zurueckgeben:
```
asdfas 1 3.4 [1, 3] <class 'int'> <class 'float'> <class 'str'> 
```

# Aufgabe 4: Mixed Parameters

Definiert eine Funktion `print_tabelle`, die als erstes Argument den Parmaeter `ueberschrift` nimmt und dann alle folgenden Named Parameter in der Variable `keyvalue` speichert. Dann soll zuerst die `ueberschrift` ausgeben werden, und danach in einer Tabelle alle Key-Value-Paare, d.h.
```python
print_tabelle("schoene tabelle", number=12123, string="Hello");
```
ergibt dann ungefaehr das:
```
schoene tabelle                                                                               
----------------------------------------------------------------------------------------------------
key                  | value                                                              
----------------------------------------------------------------------------------------------------
number               | 12123                                                              
string               | Hello                                                              
```