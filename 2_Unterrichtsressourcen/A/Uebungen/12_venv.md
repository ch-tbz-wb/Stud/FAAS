# Virtual Environment (venv)

In dieser kleinen Übung lernst du wie man per Kommandozeile ein venv erzeugt und benutzt. Das erzeugen geht ganz einfach:

```bash
python -m venv <directory of new venv>
```

Aber wie braucht man es? Dazu musst du das venv *aktivieren*:

* in der bash unter Linux/Macos/Unix:
```bash
source <directory_of_venv>/bin/activate
```
* in der git-bash unter windows:
```bash
source <directory_of_venv>/Scripts/activate
```
* im cmd oder powershell:
```powershell
.\<directory_of_venv>\Scripts\activate
```


Das setzt folgende Environment-Variablen neu:
```
$VIRTUAL_ENV
$PATH
$PYTHONHOME
```

Damit wird sichergestellt das der richtige `python`-Interpreter im Pfad ist und die richtigen Module geladen werden.

**Achtung**: Wenn ihr die Shell verlässt und ein neues Terminal startet müsst ihr wieder das environment aktivieren.

1) Erzeuge ein Virtual Enviornment unter in deinem Home-Directory im directory `test_venv`:

2) Activiere das venv
3) Führe ein python Programm aus
4) Deactiviere das venv mit `deactivate`
