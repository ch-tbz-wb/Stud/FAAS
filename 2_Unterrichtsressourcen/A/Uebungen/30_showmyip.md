


In diesem Auftrag versuchen wir einen Service, den ihr alle schon kennt, selbst zu implementieren:

[https://www.showmyip.com/](https://www.showmyip.com/)

Folgendes sollte unser Service auf jeden Fall können:

- Die IP vom Client ausgeben

Nice To Have:

- User-Agent ausgeben
- den Ganzen event als json ausgeben.

Challenge: 

- Versucht noch von der lambda Function die geolocation der IP abzufragen indem ihr ein REST-API ansprecht wie https://ip-api.com/

Vorgehen:

- myip Funktion in [AWS-Lambda](https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1) erzeugen mit dem Template "Hello world function" für Python:
  - Use existing role benutzen: LabRole
  - Code anpassen: 
    - `print("Received event: " + json.dumps(event, indent=2))` 

- Trigger hinzufügen:
  - API-Gateway auswählen:
    - Create a new API
    - HTTP API
    - Security: Open

- API endpoint im Browser aufrufen -> message	"Internal Server Error"

- Im Tab wo ihr die Lambda Funktion erzeugt habt unter  `Monitor` -> `View Cloudwatch Logs` den letzten Event anschauen. Was unter `Received event:` steht, ist das was ihr als event kriegt. Ihr werdet das Feld mit der IP finden.

- Jetzt gehts ans programmieren:
  - Ihr muesst ein Objekt der Klasse  dict aus der Funktion zurückgeben, welches ungefähr so aussieht:
    ```
    { 
      'statusCode': 200,
      'headers': { 'Content-Type' : 'text/html; charset=utf-8' },
      'body' : "Was immer euer HTML Text sein soll",
    }
    ```

- Ruft den API endpoint nochmals im Browser auf. Jetzt sollte euer Text kommen und falls ihr in diesem schon die IP eingefügt habt, auch eure IP.

- Jetzt könnt ihr noch die nice-to-have implementieren.
- Die Rest-API-GEO-Location Challenge waere auch noch was. https://ip-api.com/ 

Lösung: [Nur die Lambda Funktion](./Loesungen/lambda.md)
