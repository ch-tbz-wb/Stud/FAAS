# Übung zu Lists

## Aufgabe 1

Erstellt eine Funktion `print_type(input:list)`, welche 

- eine Liste `input` als parameter hat,
- für jedes Element der Liste den Typ und den Wert ausgibt.

D.h. `print_type(["Armin",2,3.0])` gibt als output:
```
<class 'str'>
Armin
<class 'int'>
2
<class 'float'>
3.0
```

**Tip:** um über alle Elemente einer Liste `input` zu iterieren beutzt man `for element in input:`
## Aufgabe 2

Erstellt eine Funktion `find(inputlist:list,searchelement)`, welche

- in der Liste `inputlist` nach `searchelement` sucht und eine Liste mit allen Positionen, wo `searchelement` vorkommt zurückgibt.

D.h. `print(find(["Armin",3,5,7,"Bella",7],7))` gibt folgenden output:

```
[3, 5]
```

## Aufgabe 3

Erzeuge eine Funktion `containsnumbers(liste:list)` welche überprüft ob alle Elemente der `liste` vom Typ int oder float sind und `True` oder `False` zurückgibt.

D.h. folgender Code:
```
print(containsnumbers(["ar",1,2]))
print(containsnumbers([2.0,-33,2,1,2]))
```
gibt folgenden output:
```
False
True
```

**Tip:** `type(element)` ist entweder float oder int
