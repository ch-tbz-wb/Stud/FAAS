# Übung zu Loops

## Aufgabe 1

Erstelle eine Funktion `fakultaetmitfor(n:int)`, welche folgendes berechnet

`1*2*3*.....*n`

und dies als Wert zurückgibt. Benutze einen `for`-Loop dazu.

D.h.:
```
print(fakultaetmitfor(5))
```
ergibt:
```
120
```

## Aufgabe 2

Erstelle eine Funktion `fakultaetmitwhile(n:int)`, welche folgendes berechnet

`1*2*3*.....*n`

und dies als Wert zurückgibt. Benutze einen `while`-Loop dazu.

D.h.:
```
print(fakultaetmitwhile(6))
```
ergibt:
```
720
```

## Aufgabe 3

Erstelle eine Funktion `fibonacci(n:int)`, welche die n-te Fibonacci-Zahl berechnet.

dabei gilt `fibonacci(0)==0`,`fibonacci(0)==1`, und `fibonacci(n)==fibonacci(n-1)+fibonacci(n-2)`.

**Tip:** Benutzt einen `while`-Loop und speichert die beiden vorherigen fibonacci-Zahlen in Variabeln `f_prev` und `f_prev_prev`.

D.h.
```
print(fibonacci(0))
print(fibonacci(1))
print(fibonacci(2))
print(fibonacci(3))
print(fibonacci(4))
print(fibonacci(5))
print(fibonacci(6))
```
ergibt den output:
```
0
1
1
2
3
5
8
```
## Aufgabe 4

Erstellt eine Funktion `sum_only_numbers(liste:list)`, welche alle Elemente der liste zusammenzählt falls es sich um Zahlen handelt:

D.h. der output von
```
print(sum_only_numbers([1,2.3,"i","bezug","4.5"]))
```
ist:
```
3.3
```

## Aufgabe 5

Erstellt eine Funktion `sum_only_numbers_stop(liste:list)`, welche alle Elemente der `liste` zusammenzählt, falls es sich um Zahlen handelt, bis ein Element mit einem negativen Wert kommt, dann soll abgebrochen werden.

Benutze `continue` und `break` um den Code kürzer und leserlicher zu machen.

D.h. der output von
```
print(sum_only_numbers_stop([1,2.3,"i","bezug",-1,3,3.3]))
```
ist:
```
3.3
```

## Aufgabe 6

Macht eine Funktion `is_prime(number: int)`, die testet ob `number` eine Primzahl ist. und gebt einen `boolean` zurück.

**Tip:** Eine Primazahl ist nur durch sich selbst und 1 teilbar, d.h. ihr müsst alle ganzen Zahlen bis zur Wurzel(`int(math.sqrt(number))`) der Zahl ausprobieren und falls eine die Zahl `number` teilt, ist `number` keine Primzahl.

**Beispiel:** 999 ist keine Primzahl, da sie durch 3 teilbar ist.
Die Zahl 11 ist eine Primzahl, da 11 weder durch 2 noch durch 3 teilbar ist und `int(math.sqrt(11))` ist 3, also muss ich nur 2 und 3 testen.





