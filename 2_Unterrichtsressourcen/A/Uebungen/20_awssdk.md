# AWS-SDK benutzen

## Einrichten von SDK
1) Als erstes müsst ihr ein neuen Python Projekt erstellen mit seperatem `venv`.
2) Dann müsst ihr das package `boto3` mit `pip` installieren
3) Müsste ihr noch eure credentials unter `.aws/credentials` falls ihr das noch nicht gemacht habt. (AWS-Accademy Learner-Lab)


## Auftrag 1: Listen von Subnetzen, Ssh-KeyPairs, Images(AMIs), SecurityGroups erzeugen

Beschreibungen wie man das SDK benutzt um auf EC2 zuzugreifen findet mit Beispielen unter 
[https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-examples.html](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-examples.html)   
Eine genaue Beschreibung des EC2-Clients vom SDK findet ihr unter
[https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html)

Hier noch ein Beispiel wie ihr Subnetze abfragen könnt:
```python
# SDK importieren
import boto3
# EC2-Client initialisieren
client = boto3.client('ec2')
# Subnetze abfragen
response = client.describe_subnets()
# Die Antwort ausgeben
print(response)
```

Jetzt müsst ihr nur noch die KeyPairs , Security-Groups und Images(AMIs) auflisten.

**TIP**: Bei den Images(AMIs) kann es sehr lange gehen, wenn ihr alle auflistet. Ihr könnt Filter benutzen und nach AMIs mit 'ubuntu-jammy-22.04' im Namen suchen (https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_images.html)


## Auftrag 2: VM erstellen mit den infos

Jetzt geht es darum eine VM zu erstellen. Ihr braucht die Infos aus Auftrag 1.

**TIP**: Suche nach `run_instances boto3 example`

Setze nur die Dinge, die du in Auftrag 1 rausgesucht hast und setze zusätzlich InstanceType. Du wirst dann noch Fehler kriegen aber den wirst du selbst beheben können. 

## Auftrag 3:

Versuche noch ein Cloud-Init File mitzugeben.

