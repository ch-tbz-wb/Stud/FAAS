# Classes

## Aufgabe 1: Erstellen einer Klasse

Erstellt eine Klasse `Person` mit der
- Klassenvariable `objectcount` 
- den Feldern `name`,`groesse`
- einen Konstruktor `__init__(self,name,groesse)`
- einer Methode `compare(self,other)` die self.groesse > other.groesse zurueckgeben soll.

## Aufgabe 2: Klassen benutzen

Erzeugt 2 Objekte `hans` und `peter` der Klasse `Person`:
- Peter, 168
- Hans, 185
und benutzt die Methode `compare()` um die beiden zu vergleichen und gebt einen sinnvollen Text dazu aus.


## Aufgabe 3: Erstellen einer Subklasse

Erstellt eine Klasse `Schueler` der zusätzlich zur Klasse `Person`:
- noch ein Feld `schulnote` hat.
- die Methode `compare(self,other)` soll self.schulnote > other.schulnote zurueckgeben.

## Aufgabe 4: Benutzen von gemischten Klassen

Jetzt erstellt ihr eine Liste von den Objekten aus Aufgabe 2 und einem Objekt `fritzli` und `heinz` der Klasse `Schueler` mit:

- Heinz, 145, 5.6
- Fritzli, 146, 3.1

und macht folgende Vergleiche:

fritzli.compare(heinz)
hans.compare(fritz)
fritzli.compare(hans)

Was geschieht beim letzten compare()?
Wieso? Benutzt den Debugger um herauszufinden was geschieht.


