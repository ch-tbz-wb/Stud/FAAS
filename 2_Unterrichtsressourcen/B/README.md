# Challenges und Datenstrukturen

# Objektorientiertes Programmieren

| **No.** | **Name** | 
| ------- | -------- | 
| 01 | **[Python_OOPs_Concepts](Uebungen/Vorlagen/OOP/001_Python_OOPs_Concepts.ipynb)** |
| 02 | **[Python_Classes_and_Objects](Uebungen/Vorlagen/OOP/002_Python_Classes_and_Objects.ipynb)** |
| 03 | **[Python_Inheritance](Uebungen/Vorlagen/OOP/003_Python_Inheritance.ipynb)** |
| 04 | **[Python_Operator_Overloading](Uebungen/Vorlagen/OOP/004_Python_Operator_Overloading.ipynb)** |
|    | **[self_in_Python_Demystified](Uebungen/Vorlagen/OOP/self_in_Python_Demystified.ipynb)** |

Quelle: https://github.com/milaan9/06_Python_Object_Class

# Advanced topics

| **No.** | **Name** | 
| ------- | -------- | 
| 01 | **[Python_Iterators](Uebungen/Vorlagen/ADV/001_Python_Iterators.ipynb)** |
| 02 | **[Python_Generators](Uebungen/Vorlagen/ADV/002_Python_Generators.ipynb)** |
| 03 | **[Python_Closure](Uebungen/Vorlagen/ADV/003_Python_Closure.ipynb)** |
| 04 | **[Python_Decorators](Uebungen/Vorlagen/ADV/004_Python_Decorators.ipynb)** |
|    | 4.1 **[Python_args_and_kwargs](Uebungen/Vorlagen/ADV/Python_args_and_kwargs.ipynb)** |
| 05 | **[Python_Property](Uebungen/Vorlagen/ADV/005_Python_Property.ipynb)** |
| 06 | **[Python_RegEx](Uebungen/Vorlagen/ADV/006_Python_RegEx.ipynb)** |

Quelle: https://github.com/milaan9/07_Python_Advanced_Topics

# Challenges

| # | Inhalt                                                                |
|---|-----------------------------------------------------------------------|
| 1 | [Self Evaluation - Teste deine Python-Kenntnisse](Uebungen/Vorlagen/Challenges/SelfEvaluation.ipynb)                       |
| 2 | [Building a SpaceShip](Uebungen/Vorlagen/Challenges/MyLittleSpaceship.ipynb)                       |
| 3 | [Refueling a SpaceShip](Uebungen/Vorlagen/Challenges/MyLittleSpaceshipPart2.ipynb)                       |
| 4 | [NodeWalking - Klassen, Objekte, LinkedList](Uebungen/Vorlagen/Challenges/NodeWalking.ipynb)                            |
| 5 | [Elfen im Schnee - HTTP-JSON API, Generators, "Node Walking" / Graphen](Uebungen/Vorlagen/Challenges/ElfenImSchnee.ipynb) |
| 6 | [Das Garagentor Teil 1 - MQTT, HTTP](Uebungen/Vorlagen/MQTTHTTP/DasGaragentorTeil1.ipynb) |
| 7 | [Das Garagentor Teil 2 - Logging, YAML](Uebungen/Vorlagen/MQTTHTTP/DasGaragentorTeil2.ipynb) |

# More example projects
 - [alptbz/News to Print](https://github.com/alptbz/newstoprint)
 - [ndleah/Python mini projects](https://github.com/ndleah/python-mini-project)

Autor: Philipp Albrecht

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a> and [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html). If there are any contradictions between the two licenses, the Attribution-NonCommercial-ShareAlike 4.0 International license governs. 



